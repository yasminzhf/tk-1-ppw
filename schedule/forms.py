from django import forms
from .models import Jadwal

years = [x for x in range(2020, 2050)]

class JadwalForms(forms.ModelForm):
    jadwalpelajaran = forms.CharField(widget=forms.TextInput(attrs={
        "label" : "Nama Pelajaran",
        "class" : "form-control",
        "required" : True,
        "placeholder" : "Schedule Name",
        
    }))

    deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "required" : False,
        "placeholder" : "Deskripsi",
    }))

    datetime = forms.DateField(
        widget = forms.SelectDateWidget(years = years, attrs = {'class' : 'form-control-sm'})
    )


    lokasi = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "required" : True,
        "placeholder" : "zoom",
    }))

    class Meta:
        model = Jadwal
        fields = ['jadwalpelajaran', 'deskripsi', 'datetime', 'lokasi']
