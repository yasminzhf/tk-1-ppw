from django.db import models
from django.utils import timezone

# Create your models here.
class Jadwal(models.Model):
    jadwalpelajaran = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=200)
    datetime = models.DateTimeField(default=timezone.now())
    lokasi = models.CharField(max_length=64)
    
    def __str__(self):
        return self.jadwalpelajaran

