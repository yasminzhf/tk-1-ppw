from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Jadwal
from .forms import JadwalForms

# Create your tests here.


class TestSchedule(TestCase):
    
    def test_apakah_url_jadwalmu_ada(self):
        response = self.client.get('/jadwalmu/')
        self.assertEquals(response.status_code, 200)


    def test_apakah_dihalaman_ada_text_masukkan(self):
        response = self.client.get('/jadwalmu/')
        html = response.content.decode('utf8')
        self.assertIn("Masukkan", html)


    def test_apakah_di_halaman_jadwalmu_ada_templatenya(self):
        response = self.client.get('/jadwalmu/')
        self.assertTemplateUsed(response, 'schedule/schedule.html')

    def test_schedule_model_jadwalmu(self):
        Jadwal.objects.create(jadwalpelajaran='Test jadwal')
        pelajaran = Jadwal.objects.get(jadwalpelajaran='Test jadwal')
        self.assertEqual(str(pelajaran), 'Test jadwal')


    def test_save_jadwal_a_POST_request(self):
        Jadwal.objects.create(jadwalpelajaran='PPW')
        hitung = Jadwal.objects.filter(jadwalpelajaran='PPW').count()
        self.assertEquals(hitung, 1)

    





