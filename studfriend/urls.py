from django.urls import path

from . import views

app_name = 'studfriend'

urlpatterns = [
    path('', views.index, name='index'),
]