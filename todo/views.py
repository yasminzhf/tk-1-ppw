from django.shortcuts import render,redirect
from .models import Todo
from .forms import TodoForm
# Create your views here.
def index(request):
    activity = Todo.objects.all()
    if request.method == 'POST':
        formtambah= TodoForm(request.POST)
        if formtambah.is_valid():
            formtambah.save()
            return redirect('todo:index')
    else:
        formtambah= TodoForm()
    return render(request,'todo/todo.html',{
        'activity' : activity,
        'formtambah':formtambah
    })

def tambahTodo(request):
    return render(request, "todo/todo.html")

def delete_todo(request, pk):
    activity = Todo.objects.get(pk=pk)
    activity.delete()
    return redirect('todo:index')
