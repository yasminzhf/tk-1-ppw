from django import forms
from .models import Todo

class TodoForm(forms.ModelForm):
    activity = forms.CharField(label='')
    activity.widget.attrs.update({'class':'todo-form', 'required':'required'})
    class Meta:
        model = Todo
        fields = ('activity',)