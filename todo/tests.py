from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from .urls import *
from .models import Todo

# Create your tests here.
class TodoTest(TestCase):
    def test_todo_url_is_exist(self):
        response = Client().get('/todolist/')
        self.assertEqual(response.status_code,200)

    def test_homepage_is_using_homepage_template(self):
        response = Client().get('/todolist/')
        self.assertTemplateUsed(response, 'todo/todo.html')

    def test_homepage_is_using_index_func(self):
        found = resolve('/todolist/')
        self.assertEqual(found.func, index)

    def test_tambah_todo_url_is_exist(self):
        response = Client().get('/todolist/tambah/')
        self.assertEqual(response.status_code,200)

    def test_tambah_is_using_todo_template(self):
        response = Client().get('/todolist/tambah/')
        self.assertTemplateUsed(response, 'todo/todo.html')

    def test_tambah_is_using_tambahTodo_func(self):
        found = resolve('/todolist/tambah/')
        self.assertEqual(found.func, tambahTodo)

    def test_model_can_create_new_todo(self):
        new_todo = Todo.objects.create(activity ='ngerjain TK')

        counting_all_available_activity = Todo.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_check_model_return_activity(self):
        new_todo = Todo.objects.create(activity ='ngerjain TK')
        result = Todo.objects.get(id=1)
        self.assertEqual(str(result),'ngerjain TK')

    # def test_check_delete_func(self):
    #     new_todo = Todo.objects.create(activity ='ngerjain TK')
    #     activity = Todo.objects.get(pk=1)

    #     activity.delete()

    #     counting_all_available_activity = Todo.objects.all().count()
    #     self.assertEqual(counting_all_available_activity,0)

    


