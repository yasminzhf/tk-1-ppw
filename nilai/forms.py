from .models import rata_nilai
from django import forms

class Input_Form(forms.ModelForm):
    class Meta:
        model = rata_nilai
        fields = ['nama_nilai',
        'isian_nilai',]

    isian_nilai = forms.IntegerField(label='', required=True, min_value=0, widget=forms.NumberInput(attrs={'class':'form-control','id':'isian_nilai'}))
    nama_nilai = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs={'class':'form-control','id':'nama_nilai', 'placeholder':'contoh : UTS, UAS, Latihan'}))