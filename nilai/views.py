from django.shortcuts import render, redirect
from .forms import Input_Form
from django.http import HttpResponseRedirect, HttpResponse
from .models import rata_nilai

# Create your views here.
respons = {}
def index(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return redirect('/nilaimu/daftar_nilai')
    respons['input_form'] = form
    return render(request, 'nilai/input_nilai.html', respons)

def daftar_nilai(request):
    tipe_nilai = rata_nilai.objects.all()
    respons['tipe_nilai'] = tipe_nilai
    return render(request,'nilai/daftar_nilai.html', respons)

def hapus(request, hapus_id):
    rata_nilai.objects.filter(id=hapus_id).delete()
    return redirect('/nilaimu/daftar_nilai')