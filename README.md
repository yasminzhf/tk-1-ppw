# Tugas Kelompok 1 PPW
| No.  |           Nama          | NPM  | Tugas |
| ---- | ----------------------- | ---- | ----- |
| 1    | Muhammad Farhan Thariq  |      |       |
| 2    | Yasmin Q. A             |      |       |
| 3    | Yasmin Zhafira Fadhila  |      |       |
| 4    | Marthin Lubis           |      |       |


## status 
[![pipeline status](https://gitlab.com/yasminzhf/tk-1-ppw/badges/master/pipeline.svg)](https://gitlab.com/yasminzhf/tk-1-ppw/-/commits/master)


[![coverage report](https://gitlab.com/yasminzhf/tk-1-ppw/badges/master/coverage.svg)](https://gitlab.com/yasminzhf/tk-1-ppw/-/commits/master)


## link herokuapp
studfriend.herokuapp.com

## Cerita aplikasi 

Dimasa pandemi saat ini, kalangan terpelajar mulai dari SD,SMP,SMA, bahkan Perguruan tinggi menerapkan pembelajaran secara daring (online) untuk mencegah penularan COVID-19 yang sedang mewabah di seluruh penjuru negeri. Mereka dituntut untuk mandiri di berbagai hal seperti belajar dan membuat tugas. Tak hanya itu, mereka yang melaksanakan pembelajaran daring di rumah juga harus berhadapan dengan lingkungan rumah yang mungkin tidak mendukung tatkala sedang belajar. Untuk itu kami ingin merancang halaman web untuk membantu para kaum terpelajar agar memudahkan mereka dalam memanajemen waktu dengan baik. 

## Daftar fitur 
Lihat nilai
Di dalam fitur ini pelajar bisa memasukkan  nilai kedalam website dan website akan memberitahu indeks nilai seperti A, B, C, D atau E. Serta memberikan grafik persebaran nilai yang menunjukan nilai naik atau turun. 
Jadwal
Pelajar bisa memasukkan, mengubah, dan menghapus jadwal mereka ke dalam website. 
List ceklis tugas
Pelajar bisa memasukkan, menambah, dan menghapus  list tugas mereka ke dalam aplikasi web.
Notes
Pelajar bisa menulis apapun disini sebagai pengingat/catatan.

## Persona
Target pengguna web kami adalah para pelajar. tidak menutup kemungkinan pelajar SMP, SMA, maupun mahasiswa.

Untuk pemilihan warna, kami memilih background putih. Hal ini bertujuan untuk memberi kesan calm saat melihat aplikasi. Selain itu kami akan memainkan kombinasi warna yang vibrant untuk  fitur fitur yang ada. Tujuan pemilihan warna vibrant yang kontras dengan background putih bertujuan untuk memberikan kesan pop up dari fitur yang tersedia. Selain itu, kami memilih penggunaan animasi animasi gambar agar web yang dibuat tidak terlalu flat. 
